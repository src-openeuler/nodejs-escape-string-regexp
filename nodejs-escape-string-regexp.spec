%{?nodejs_find_provides_and_requires}
%global packagename escape-string-regexp
%global enable_tests 0
Name:		nodejs-escape-string-regexp
Version:	1.0.5
Release:	3
Summary:	Escape RegExp special characters
License:	MIT
URL:		https://github.com/sindresorhus/escape-string-regexp
Source0:	https://registry.npmjs.org/%{packagename}/-/%{packagename}-%{version}.tgz
Source1:	https://raw.githubusercontent.com/sindresorhus/escape-string-regexp/v%{version}/test.js
ExclusiveArch:	%{nodejs_arches} noarch
BuildArch:	noarch
BuildRequires:       	nodejs-packaging
%if 0%{?enable_tests}
BuildRequires:       	npm(ava)
%endif
%description
Escape RegExp special characters

%prep
%setup -q -n package
cp -p %{SOURCE1} .

%build

%install
mkdir -p %{buildroot}%{nodejs_sitelib}/%{packagename}
cp -pr package.json index.js \
	%{buildroot}%{nodejs_sitelib}/%{packagename}
%nodejs_symlink_deps

%check
%nodejs_symlink_deps --check
%{__nodejs} -e 'require("./")'
%if 0%{?enable_tests}
%{_bindir}/ava
%else
%{_bindir}/echo -e "\e[101m -=#=- Tests disabled -=#=- \e[0m"
%endif

%files
%{!?_licensedir:%global license %doc}
%doc *.md
%license license
%{nodejs_sitelib}/%{packagename}

%changelog
* Mon Sep 9 2024 yuanlipeng <yuanlipeng@huawei.com> - 1.0.5-3
- fix specfile error in changelog

* Thu Nov 26 2020 leiju <leiju4@huawei.com> - 1.0.5-2
- delete nodejs-escape-string-regexp.spec.old redundancy file

* Thu Aug 20 2020 Anan Fu <fuanan3@huawei.com> - 1.0.5-1
- package init
